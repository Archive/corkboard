from corkboard.pinup import Pinup
import gtk

class FilePinup(Pinup):
    def __init__(self, uri, parent_canvas_group):
        Pinup.__init__(self, parent_canvas_group)
        
        if self.__class__ == FilePinup:
            assert("FilePinup is an abstract class")
