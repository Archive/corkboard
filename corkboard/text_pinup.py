from corkboard.pinup import Pinup
import gtk

class TextPinup(Pinup):
    def __init__(self, text, parent_canvas_group):
        Pinup.__init__(self, parent_canvas_group)
        item = self.get_canvas_group().add('GnomeCanvasRichText',
                                           text=text,
                                           wrap_mode=gtk.WRAP_WORD_CHAR)

