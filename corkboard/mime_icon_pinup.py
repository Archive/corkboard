from corkboard.file_pinup import FilePinup

class MimeIconPinup(FilePinup):
    def __init__(self, uri, parent_canvas_group):
        FilePinup.__init__(self, uri, parent_canvas_group)
        
        item = self.get_canvas_group().add('GnomeCanvasText',
                                           x=0, y=0,
                                           text=uri,
                                           fill_color='black')
