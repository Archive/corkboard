import gtk
import gnome.canvas
from corkboard.cork import Cork

(NORTH, EAST, SOUTH, WEST) = range(4)
    
class Board(gtk.Window):

    def __init__(self):
        gtk.Window.__init__(self)

        # Set basic properties to defaults
        self._time_to_extend = 0.2
        self._frames_per_second = 40
        self._corkboard_extended = False
        self._corkboard_percent_extension = 0.0
        self._corkboard_thickness = 50
        self._side_of_screen = EAST
        
        # Add the window contents
        self._contents = Cork()
        self.add(self._contents)

        # Set window properties
        self.set_type_hint(gtk.gdk.WINDOW_TYPE_HINT_DOCK)
        self.set_decorated(False)
        self.stick()
        self._position()
        
        self.show_all()

    def set_extended(self, corkboard_extended):
        self._corkboard_extended = corkboard_extended
        gtk.timeout_add(1000 / self._frames_per_second, self._animate_corkboard_extension)

    def _animate_corkboard_extension(self):
        percentage_per_tick = 1.0 / (self._time_to_extend * (float)(self._frames_per_second))
        if self._corkboard_extended:
            self._corkboard_percent_extension = self._corkboard_percent_extension + percentage_per_tick
            if self._corkboard_percent_extension > 1.0:
                self._corkboard_percent_extension = 1.0
                tick_again = False
            else:
                tick_again = True
        else:
            self._corkboard_percent_extension = self._corkboard_percent_extension - percentage_per_tick
            if self._corkboard_percent_extension < 0.0:
                self._corkboard_percent_extension = 0.0
                tick_again = False
            else:
                tick_again = True

        self._position()
        
        return tick_again


    def _compute_position(self):
        screen = self.get_screen()
        screen_height = screen.get_height()
        screen_width  = screen.get_width()
        
        if self._side_of_screen == NORTH:
            corkboard_width = screen_width
            corkboard_height = self._corkboard_thickness
            x_extension_direction = 0
            y_extension_direction = 1
            unextended_x = 0
            unextended_y = -1 * self._corkboard_thickness
        elif self._side_of_screen == EAST:
            corkboard_width = self._corkboard_thickness
            corkboard_height = screen_height
            x_extension_direction = -1
            y_extension_direction = 0
            unextended_x = screen_width
            unextended_y = 0
        elif self._side_of_screen == SOUTH:
            corkboard_width = screen_width
            corkboard_height = self._corkboard_thickness
            x_extension_direction = 0
            y_extension_direction = -1
            unextended_x = 0
            unextended_y = screen_height
        elif self._side_of_screen == WEST:
            corkboard_width = self._corkboard_thickness
            corkboard_height = screen_height
            x_extension_direction = 1
            y_extension_direction = 0
            unextended_x = -1 * self._corkboard_thickness
            unextended_y = 0

        corkboard_pixel_extension = int(self._corkboard_percent_extension * self._corkboard_thickness)
        x_extension_pixels = corkboard_pixel_extension * x_extension_direction
        y_extension_pixels = corkboard_pixel_extension * y_extension_direction

        x = x_extension_pixels + unextended_x
        y = y_extension_pixels + unextended_y

        return (corkboard_width, corkboard_height, x, y)

    def _position(self):
        (corkboard_width, corkboard_height, x, y) = self._compute_position()

        self.move(x, y)
        self.set_size_request(corkboard_width, corkboard_height)

    def get_screen_geometry(self):
        return (screen.get_width(), screen.get_height())
