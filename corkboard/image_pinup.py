from corkboard.file_pinup import FilePinup
import gtk
import gnome.ui

class ImagePinup(FilePinup):
    def __init__(self, uri, parent_canvas_group):
        FilePinup.__init__(self, uri, parent_canvas_group)
        
        pixbuf = gnome.ui.gdk_pixbuf_new_from_uri(uri)
        item = self.get_canvas_group().add('GnomeCanvasPixbuf', pixbuf=pixbuf)


_supported_mime_types = []
formats = gtk.gdk.pixbuf_get_formats()
for format in formats:
    _supported_mime_types = _supported_mime_types + format['mime_types']

def get_supported_mime_types():
    return _supported_mime_types    
