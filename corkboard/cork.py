import gtk
import gnome.canvas

from corkboard.pinup import PinupFactory

class Cork(gtk.EventBox):
    
    def __init__(self):
        gtk.EventBox.__init__(self)

        self._pinups = []
        self._pinup_size = 48

        self._canvas = gnome.canvas.Canvas(aa=True)
        self._background_group = self._canvas.root().add('GnomeCanvasGroup', x=0, y=0)
        self._background_rect = self._background_group.add('GnomeCanvasRect', fill_color='red', outline_color='black')
        self._contents_group = self._canvas.root().add('GnomeCanvasGroup', x=0, y=0)

        self._pinup_factory = PinupFactory(self._contents_group)
        
        self._canvas.show()
        self.add(self._canvas)

        self._canvas.connect('drag_data_received', self._drag_data_received_cb)
        self._canvas.drag_dest_set(gtk.DEST_DEFAULT_ALL,
                                   [], gtk.gdk.ACTION_MOVE)
        gtk.drag_dest_add_uri_targets(self._canvas)
        gtk.drag_dest_add_text_targets(self._canvas)

        self.connect("size_allocate", self._size_allocate_cb)
        
    def _size_allocate_cb(self, widget, allocation):
        print (allocation.x, allocation.y, allocation.width, allocation.height)
        self._size_canvas(allocation.width, allocation.height)

    def _size_canvas(self, width, height):
        print (width, height)
        self._canvas.set_scroll_region(0, 0, width, height)
        self._background_rect.set(x1=0, y1=0, x2=width, y2=height)

    def _add_pinups(self, pinups):
        self._pinups = self._pinups + pinups
        self._layout_pinups()

    def _layout_pinups(self):
        i = 0
        for pinup in self._pinups:
            pinup.set_location(0, i * (self._pinup_size + 5))
            pinup.set_max_size(self._pinup_size, self._pinup_size)
            i = i + 1
        
    def _drag_data_received_cb(self, widget, context, x, y, selection, target_type, time):
        self._add_pinups(self._pinup_factory.new_pinups_from_selection(selection))
