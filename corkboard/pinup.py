import gtk
import gnome.canvas
import gnome.vfs

class Pinup:
    def __init__(self, parent_canvas_group):
        self._pinup_canvas_group = parent_canvas_group.add('GnomeCanvasGroup', x=0, y=0)

    def get_canvas_group(self):
        return self._pinup_canvas_group

    def set_location(self, x, y):
        print "Location is %d,%d" % (x, y)
        self.get_canvas_group().set(x=x, y=y)

    def set_max_size(self, max_width, max_height):
        (x1, y1, x2, y2) = self.get_canvas_group().get_bounds()
        current_width = abs(x1 - x2)
        current_height = abs(y1 - y2)

        if current_width == 0.0 or current_height == 0.0:
            print ("one of the dimensions is 0")
            return

        width_ratio = float(max_width) / float(current_width)
        print ("width ratio is ", width_ratio)
        height_ratio = float(max_height) / float(current_height)
        print ("height ratio is ", height_ratio)
        if width_ratio > height_ratio:
            ratio = height_ratio
        else:
            ratio = width_ratio

        print ("using ratio %f" % (ratio))

        self.get_canvas_group().affine_relative((ratio, 0.0, 0.0, ratio, 0.0, 0.0))

from corkboard.image_pinup import ImagePinup
import corkboard.image_pinup
from corkboard.mime_icon_pinup import MimeIconPinup
from corkboard.text_pinup import TextPinup

class PinupFactory:
    def __init__(self, canvas_group):
        self._canvas_group = canvas_group

    def new_pinups_from_selection(self, selection):
        new_pinups = []
        
        uris = selection.get_uris()
        for uri in uris:
            new_pinups.append(self.new_pinup_from_uri(uri))

        text = selection.get_text()
        if text:
            new_pinups.append(self.new_pinup_from_text(text))

        return new_pinups

    def new_pinup_from_uri(self, uri):
        print ("Adding URI '%s'" % (uri))
        info = gnome.vfs.get_file_info(uri, options=gnome.vfs.FILE_INFO_GET_MIME_TYPE)
        if info.mime_type in corkboard.image_pinup.get_supported_mime_types():
            return ImagePinup(uri, self._canvas_group)
        else:
            return MimeIconPinup(uri, self._canvas_group)

    def new_pinup_from_text(self, text):
        print ("Adding text '%s'" % (text)) 
        return TextPinup(text, self._canvas_group)
